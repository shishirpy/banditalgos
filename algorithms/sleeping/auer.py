# -*- coding: utf-8 -*-
"""
author : Shishir Pandey
email : shishir.py@gmail.com


 This is an implementation of the Awake Upper Estimated Reward
(AUER). The algorithm is presented in the paper - 

1. Regret Bounds for Sleeping Experts and Bandits
   - Rober D. Kleinberg and Alexandru Niculescu-Mizil and 
     Yogeshwar Sharma

"""

import random
import math
from ..arms import bernoulli_arm


def ind_max(x):
    m = max(x)
    return x.index(m)


class AUER():
    """
    To make things simple we will have the following ordering on expected 
    reward of 
    p1 >= p2 >= ... >= pK
    
    where p1, p2, ... , pK represent the expected reward for arms 
    a1, a2, ..., aK respectively.
    
    Hence, the best arm to be played at everytime step t will the arm 
    with the smallest index.
    
    This will help us calculate the regret easily but the algorithm itself
    does not make use of this ordering.
    """
    def __init__(self, counts, values):
        self.counts = counts
        self.values = values
        self.active_sets = list()
        self.n_arms = 0
        self.best_arm = list()
        return

    
    def initialize(self, n_arms):
        self.counts = [0 for col in range(n_arms)]
        self.values = [0.0 for col in range(n_arms)]
        self.n_arms = n_arms
        return

    
    def select_active_set(self):
        n_items = random.randint(1, len(self.count) - 1)
        active_arms = random.sample(range(self.n_arms), n_items)
        self.active_sets.append(active_arms)
        self.__best_arm()
        return
        
        
    def __best_arm(self):
        """
        This function will return the best arm for the present active set.
        This is mainly used to calculate the regret of the algorithm.
        """
        self.best_arm.append(min(self.active_sets[-1]))


    def select_arm(self):
        active_arms = self.active_sets[-1]
        n_arms = len(active_arms)

        for arm in active_arms:
            if self.counts[arm] == 0:
                return arm

        ucb_values = [0.0 for arm in range(n_arms)]
        total_counts = sum(self.counts)

        for arm in active_arms:
            bonus = math.sqrt((8 * math.log(total_counts))/float(self.count[arm]))
            ucb_values[arm] = self.values[arm] + bonus

        return ind_max(ucb_values)


    def update(self, chosen_arm, reward):
        self.counts[chosen_arm] += 1
        n = self.counts[chosen_arm]

        value = self.values[chosen_arm]
        new_value = ((n - 1) / float(n)) * value + (1 / float(n)) * reward
        self.values[chosen_arm] = new_value
        return
        
        
        
def test_algorithm(algo, arms, num_sims, horizon):
    chosen_arms = [0.0 for i in range(num_sims * horizon)]
    rewards = [0.0 for i in range(num_sims * horizon)]
    cumulative_rewards = [0.0 for i in range(num_sims * horizon)]
    sim_nums = [0.0 for i in range( num_sims * horizon)]
    times = [0.0 for i in range(num_sims * horizon)]


    for sim in range(num_sims):
        sim = sim + 1
        algo.initialize(len(arms))

        for t in range(horizon):
            t += 1
            index = (sim -1 ) * horizon + t - 1

            sim_nums[index] = sim
            times[index] = t

            chosen_arm = algo.select_arm()
            chosen_arms[index] = chosen_arm

            reward = arms[chosen_arms[index]].draw()
            rewards[index] = reward

            if t == 1:
                cumulative_rewards[index] = reward
            else:
                cumulative_rewards[index] = cumulative_reward[index -1] +reward

            algo.update(chosen_arm, reward)


        return [sim_nums, times, chosen_arms, rewards, cumulative_rewards]


if __name__ == "__main__":
    random.random()
